import time
from unittest.mock import patch

import pytest

from oidc_client.config import ProviderConfig
from oidc_client.error import TokenRequestParamError
from oidc_client.lib import login
from oidc_client.oauth import TokenResponse

from .utils import asio, random_string

PROVIDER_CONFIG = ProviderConfig(
    "https://example.com",
    authorization_endpoint="https://example.com/oauth2/authorize",
    token_endpoint="https://example.com/oauth2/token",
)


def test_login_non_interactive():
    token_response = TokenResponse(
        access_token=f"token:{random_string()}",
        id_token=f"id_token:{random_string()}",
        token_type="Bearer",
        expires_in=30,
        created_at=int(time.time()),
        scope="openid profile email api",
    )

    with patch("oidc_client.oauth.urlopen", return_value=asio(token_response)):
        assert (
            login(
                PROVIDER_CONFIG,
                client_id="my-app",
                client_secret="secret",
                interactive=False,
            )
            == token_response
        )


def test_login_non_interactive_no_client_secret():
    with pytest.raises(TokenRequestParamError) as excinfo:
        login(PROVIDER_CONFIG, client_id="my-app", interactive=False)
    assert "client_secret must be provided" in str(excinfo.value)


def test_login_interactive():
    token_response = TokenResponse(
        access_token=f"token:{random_string()}",
        id_token=f"id_token:{random_string()}",
        token_type="Bearer",
        expires_in=30,
        created_at=int(time.time()),
        scope="openid profile email api",
    )
    redirect_uri = "https://localhost:39303/oauth2/callback"

    with (
        patch(
            "oidc_client.lib.start_authorization_code_flow",
            return_value=random_string(),
        ),
        patch("oidc_client.oauth.urlopen", return_value=asio(token_response)),
    ):
        assert (
            login(
                PROVIDER_CONFIG,
                client_id="my-app",
                redirect_uri=redirect_uri,
                interactive=True,
            )
            == token_response
        )


def test_login_interactive_no_redirect_uri():
    with pytest.raises(TokenRequestParamError) as excinfo:
        login(PROVIDER_CONFIG, client_id="my-app", interactive=True)
    assert "redirect_uri must be provided" in str(excinfo.value)
