import io
import json
import random
import string
from dataclasses import asdict


def asio(dataclass):
    """Return dataclass as buffered I/O."""
    return io.BytesIO(json.dumps(asdict(dataclass)).encode())


def random_string():
    return "".join(random.choices(string.ascii_letters + string.digits, k=16))
