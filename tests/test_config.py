from importlib.resources import as_file, files

import pytest

from oidc_client.config import (
    DEFAULT_OIDC_SCOPE,
    DEFAULT_REDIRECT_URI,
    ClientProfile,
    read_profile,
    validate_redirect_uri,
)
from oidc_client.error import ProviderConfigError


@pytest.mark.parametrize(
    "redirect_uri",
    [
        "http://127.0.0.1:39303",
        "https://localhost:39303",
        "http://127.0.0.1:39303/oauth2/callback",
        "https://localhost:39303/oauth2/callback",
    ],
)
def test_validate_redirect_uri_ok(redirect_uri):
    validate_redirect_uri(redirect_uri)


@pytest.mark.parametrize(
    "redirect_uri",
    [
        "127.0.0.1:39303",
        "localhost:39303",
        "tcp://localhost:39303",
        "tcp://127.0.0.1:39303",
    ],
)
def test_validate_redirect_uri_bad_scheme(redirect_uri):
    with pytest.raises(ProviderConfigError) as excinfo:
        validate_redirect_uri(redirect_uri)
    assert "redirect scheme" in str(excinfo.value)


@pytest.mark.parametrize(
    "redirect_uri",
    [
        "http://127.0.0.1",
        "https://localhost",
        "https://localhost/oauth2/callback",
        "http://127.0.0.1/oauth2/callback",
        "http://:39303",
        "https://:39303",
        "http://:39303/oauth2/callback",
        "https://:39303/oauth2/callback",
    ],
)
def test_validate_redirect_uri_no_hostname_port(redirect_uri):
    with pytest.raises(ProviderConfigError) as excinfo:
        validate_redirect_uri(redirect_uri)
    assert "must include hostname and port" in str(excinfo.value)


@pytest.mark.parametrize(
    "redirect_uri",
    [
        "http://127.0.0.1:39303?test=true",
        "https://localhost:39303?test=true",
        "http://127.0.0.1:39303/?test=true",
        "https://localhost:39303/?test=true",
        "http://127.0.0.1:39303/oauth2/callback?test=true",
        "https://localhost:39303/oauth2/callback?test=true",
    ],
)
def test_validate_redirect_uri_forbidden_qs(redirect_uri):
    with pytest.raises(ProviderConfigError) as excinfo:
        validate_redirect_uri(redirect_uri)
    assert "must not include query params" in str(excinfo.value)


@pytest.mark.parametrize(
    "redirect_uri",
    [
        "http://localhost:39303",
        "http://localhost:39303/oauth2/callback",
    ],
)
def test_validate_redirect_no_tls_non_loopback(redirect_uri):
    with pytest.raises(ProviderConfigError) as excinfo:
        validate_redirect_uri(redirect_uri)
    assert "TLS must be enabled" in str(excinfo.value)


def test_client_profile_minimal():
    test_issuer = "https://example.com"
    test_client_id = "test-app"

    client_profile = ClientProfile(issuer=test_issuer, client_id=test_client_id)

    assert client_profile.issuer == test_issuer
    assert client_profile.client_id == test_client_id
    assert client_profile.client_secret is None
    assert client_profile.redirect_uri == DEFAULT_REDIRECT_URI
    assert client_profile.scope == DEFAULT_OIDC_SCOPE


def test_client_profile_full():
    test_issuer = "https://example.com"
    test_client_id = "test-app"
    test_client_secret = "test!secret"
    test_redirect_uri = "https://localhost:39303"
    test_scope = "custom api"
    test_audience = "https://api.example.com"

    client_profile = ClientProfile(
        issuer=test_issuer,
        client_id=test_client_id,
        client_secret=test_client_secret,
        redirect_uri=test_redirect_uri,
        scope=test_scope,
        audience=test_audience,
    )

    assert client_profile.issuer == test_issuer
    assert client_profile.client_id == test_client_id
    assert client_profile.client_secret == test_client_secret
    assert client_profile.redirect_uri == test_redirect_uri
    assert client_profile.scope == test_scope
    assert client_profile.audience == test_audience


@pytest.mark.parametrize("issuer", ["http://example.com", "example.com"])
def test_client_profile_bad_issuer(issuer):
    with pytest.raises(ProviderConfigError) as excinfo:
        ClientProfile(issuer="test-app", client_id=issuer)
    assert "must be HTTPS" in str(excinfo.value)


def test_read_profile_missing():
    with pytest.raises(ProviderConfigError):
        read_profile("no_such_file.toml", "no_such_profile")


@pytest.mark.parametrize(
    "profile_name,profile",
    [
        # Default profile
        (None, ClientProfile("https://default.example.com", "default-test-app")),
        # Minimal profile
        (
            "test_minimal",
            ClientProfile("https://minimal.example.com", "minimal-test-app"),
        ),
        # Full profile
        (
            "test_full",
            ClientProfile(
                "https://full.example.com",
                "full-test-app",
                client_secret="test!secret;full",
                redirect_uri="https://localhost:39303",
                scope="openid user",
                audience="https://api.example.com",
            ),
        ),
    ],
)
def test_read_profile(profile_name, profile):
    with as_file(files(__package__).joinpath("profiles.toml")) as config_file:
        assert read_profile(config_file, profile_name) == profile
