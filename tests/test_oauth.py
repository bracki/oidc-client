import io
import json
import ssl
import time
from dataclasses import asdict
from functools import partial
from http import HTTPStatus
from unittest.mock import patch
from urllib.error import HTTPError
from urllib.parse import urlencode
from urllib.request import urlopen

import pytest

from oidc_client.error import AuthorizationError, RedirectionError
from oidc_client.oauth import (
    GrantType,
    TokenResponse,
    fetch_token,
    open_authorization_endpoint,
    redirection_server,
    start_authorization_code_flow,
)
from oidc_client.pkce import PKCESecret

from .utils import asio, random_string


@pytest.fixture
def tls_context_localhost():
    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE
    return ctx


@pytest.mark.parametrize(
    "grant_args,extra_args",
    [
        (
            {
                "client_id": "my-app",
                "grant_type": GrantType.CLIENT_CREDENTIALS,
                "client_secret": "secret",
            },
            {},
        ),
        (
            {
                "client_id": "my-app",
                "grant_type": GrantType.CLIENT_CREDENTIALS,
                "client_secret": "secret",
                "audience": "https://api.example.com/",
            },
            {},
        ),
        (
            {
                "client_id": "my-app",
                "grant_type": GrantType.CLIENT_CREDENTIALS,
                "client_secret": "secret",
            },
            {
                "code": f"code:{random_string()}",
                "redirect_uri": "https://localhost:39303/oauth2/callback",
                "pkce_secret": PKCESecret(),
            },
        ),
        (
            {
                "client_id": "my-app",
                "grant_type": GrantType.AUTHORIZATION_CODE,
                "code": f"code:{random_string()}",
                "redirect_uri": "https://localhost:39303/oauth2/callback",
                "pkce_secret": PKCESecret(),
            },
            {},
        ),
        (
            {
                "client_id": "my-app",
                "grant_type": GrantType.AUTHORIZATION_CODE,
                "code": f"code:{random_string()}",
                "redirect_uri": "https://localhost:39303/oauth2/callback",
                "pkce_secret": PKCESecret(),
            },
            {
                "client_secret": "secret",
            },
        ),
    ],
)
def test_fetch_token_ok(grant_args, extra_args):
    endpoint = "https://example.com/oauth2/token"
    token_response = TokenResponse(
        access_token=f"token:{random_string()}",
        id_token=f"id_token:{random_string()}",
        token_type="Bearer",
        expires_in=30,
        created_at=int(time.time()),
        scope="openid profile email api",
    )

    with (
        patch(
            "oidc_client.oauth.urlopen", return_value=asio(token_response)
        ) as mock_urlopen,
        patch("oidc_client.oauth.Request") as mock_request,
    ):
        # Pass a mix of `grant_args` (used) and `extra_args` (unused)
        assert fetch_token(endpoint, **grant_args, **extra_args) == token_response

        # Format grant args passed to `fetch_token()` into token endpoint grant params
        try:
            grant_args["code_verifier"] = grant_args.pop("pkce_secret")
        except KeyError:
            pass
        grant_params = urlencode(
            {key: format(value) for key, value in sorted(grant_args.items())}
        ).encode()

        # Ensure the request is configured only with grant params
        mock_request.assert_called_once_with(endpoint, data=grant_params)

        # Ensure we make the request
        mock_urlopen.assert_called_once()


@pytest.mark.parametrize("grant_type", [None, "unsupported"])
def test_fetch_token_bad_grant_type(grant_type):
    with pytest.raises(ValueError) as excinfo:
        fetch_token(
            "https://example.com/oauth2/token",
            client_id="test-client",
            grant_type=grant_type,
        )
    assert "unsupported grant type" in str(excinfo.value)


@pytest.mark.parametrize(
    "grant_args",
    [
        {},
        {"redirect_uri": None, "code": None, "pkce_secret": None},
        {
            "redirect_uri": "https://localhost:39303",
            "code": random_string(),
            "pkce_secret": None,
        },
        {"redirect_uri": None, "code": random_string(), "pkce_secret": PKCESecret()},
        {
            "redirect_uri": "https://localhost:39303",
            "code": None,
            "pkce_secret": PKCESecret(),
        },
        {"redirect_uri": "https://localhost:39303", "code": None, "pkce_secret": None},
        {"redirect_uri": None, "code": None, "pkce_secret": PKCESecret()},
        {"redirect_uri": None, "code": random_string(), "pkce_secret": None},
    ],
)
def test_fetch_token_bad_authorization_code_grant_args(grant_args):
    with pytest.raises(TypeError) as excinfo:
        fetch_token(
            "https://example.com/oauth2/token",
            client_id="test-client",
            grant_type=GrantType.AUTHORIZATION_CODE,
            **grant_args,
        )
    assert "requires both code and redirect URI" in str(excinfo.value)
    assert "only allowed with PKCE" in str(excinfo.value)


@pytest.mark.parametrize(
    "grant_args",
    [{}, {"client_secret": None}],
)
def test_fetch_token_bad_client_credentials_grant_args(grant_args):
    with pytest.raises(TypeError) as excinfo:
        fetch_token(
            "https://example.com/oauth2/token",
            client_id="test-client",
            grant_type=GrantType.CLIENT_CREDENTIALS,
            **grant_args,
        )
    assert "requires a client secret" in str(excinfo.value)


@pytest.mark.parametrize(
    "token_response",
    [
        {},
        {"hello": "world"},
        {"access_token": f"token:{random_string()}"},
        {"token_type": "Bearer"},
    ],
)
def test_fetch_token_bad_token_response(token_response):
    with (
        patch(
            "oidc_client.oauth.urlopen",
            return_value=io.BytesIO(json.dumps(token_response).encode()),
        ),
        pytest.raises(AuthorizationError),
    ):
        fetch_token(
            "https://example.com/oauth2/token",
            client_id="test-client",
            client_secret="secret",
            grant_type=GrantType.CLIENT_CREDENTIALS,
        )


def test_fetch_token_ignore_token_response_extra_key():
    token_response = TokenResponse(
        access_token=f"token:{random_string()}",
        id_token=f"id_token:{random_string()}",
        token_type="Bearer",
        expires_in=30,
        created_at=int(time.time()),
        scope="openid profile email api",
    )

    with patch(
        "oidc_client.oauth.urlopen",
        return_value=io.BytesIO(
            json.dumps({**asdict(token_response), "extra_key": "extra_value"}).encode()
        ),
    ):
        assert (
            fetch_token(
                "https://example.com/oauth2/token",
                client_id="test-client",
                client_secret="secret",
                grant_type=GrantType.CLIENT_CREDENTIALS,
            )
            == token_response
        )


def test_fetch_token_http_error():
    with (
        patch(
            "oidc_client.oauth.urlopen",
            side_effect=HTTPError(
                code=400,
                msg="Not found",
                hdrs={},
                url="https://localhost:39303",
                fp=io.BytesIO(json.dumps({"error": "bad request"}).encode()),
            ),
        ),
        pytest.raises(AuthorizationError),
    ):
        fetch_token(
            "https://example.com/oauth2/token",
            client_id="test-client",
            client_secret="secret",
            grant_type=GrantType.CLIENT_CREDENTIALS,
        )


def test_open_authorization_endpoint():
    with patch("oidc_client.oauth.webbrowser.open") as mock_webbrowser_open:
        endpoint = "https://example.com/oauth2/authorize"
        client_id = "my-app"
        redirect_uri = "https://localhost:39303"
        state = random_string()
        scope = "openid profile email"
        pkce_secret = PKCESecret()

        open_authorization_endpoint(
            endpoint, client_id, redirect_uri, state, scope, pkce_secret
        )

        params = {
            "client_id": client_id,
            "redirect_uri": redirect_uri,
            "response_type": "code",
            "state": state,
            "scope": scope,
            "code_challenge": pkce_secret.challenge,
            "code_challenge_method": pkce_secret.challenge_method,
        }
        mock_webbrowser_open.assert_called_once_with(f"{endpoint}?{urlencode(params)}")


@pytest.mark.timeout(2)
@pytest.mark.parametrize("scheme", ["http", "https"])
@pytest.mark.asyncio
async def test_handle_authorization_code_ok(
    event_loop, unused_tcp_port, scheme, tls_context_localhost
):
    redirect_uri = f"{scheme}://127.0.0.1:{unused_tcp_port}"
    expected_code = random_string()

    with redirection_server(redirect_uri) as httpd:
        event_loop.run_in_executor(None, httpd.handle_request)
        response = urlopen(
            f"{redirect_uri}?"
            f"{urlencode({'state': httpd.state, 'code': expected_code})}",
            timeout=5.0,
            context=tls_context_localhost,
        )

    assert httpd.code == expected_code
    assert response.code == HTTPStatus.OK


@pytest.mark.timeout(2)
@pytest.mark.parametrize("scheme", ["http", "https"])
@pytest.mark.asyncio
async def test_handle_authorization_code_bad_path(
    event_loop, unused_tcp_port, scheme, tls_context_localhost
):
    bad_path = "/bad/path"
    redirect_uri = f"{scheme}://127.0.0.1:{unused_tcp_port}"

    with redirection_server(redirect_uri) as httpd:
        event_loop.run_in_executor(None, httpd.handle_request)

        with pytest.raises(HTTPError) as excinfo:
            urlopen(
                f"{redirect_uri}{bad_path}"
                f"?{urlencode({'state': httpd.state, 'code': random_string()})}",
                timeout=5.0,
                context=tls_context_localhost,
            )

            assert excinfo.code == HTTPStatus.NOT_FOUND


@pytest.mark.timeout(2)
@pytest.mark.parametrize("scheme", ["http", "https"])
@pytest.mark.asyncio
async def test_handle_authorization_code_bad_state(
    event_loop, unused_tcp_port, scheme, tls_context_localhost
):
    redirect_uri = f"{scheme}://127.0.0.1:{unused_tcp_port}"
    bad_state = random_string() + "="

    with redirection_server(redirect_uri) as httpd:
        assert httpd.state != bad_state

        event_loop.run_in_executor(None, httpd.handle_request)

        with pytest.raises(HTTPError) as http_excinfo:
            urlopen(
                f"{redirect_uri}"
                f"?{urlencode({'state': bad_state, 'code': random_string()})}",
                timeout=5.0,
                context=tls_context_localhost,
            )

            assert http_excinfo.code == HTTPStatus.BAD_REQUEST
            assert "bad state" in http_excinfo.reason

    assert isinstance(httpd.error, RedirectionError)


@pytest.mark.timeout(2)
@pytest.mark.parametrize("scheme", ["http", "https"])
@pytest.mark.parametrize("missing_state", [{}, {"state": ""}])
@pytest.mark.asyncio
async def test_handle_authorization_code_no_state(
    event_loop, unused_tcp_port, scheme, missing_state, tls_context_localhost
):
    redirect_uri = f"{scheme}://127.0.0.1:{unused_tcp_port}"

    with redirection_server(redirect_uri) as httpd:
        event_loop.run_in_executor(None, httpd.handle_request)

        with pytest.raises(HTTPError) as excinfo:
            urlopen(
                f"{redirect_uri}"
                f"?{urlencode({**missing_state, 'code': random_string()})}",
                timeout=5.0,
                context=tls_context_localhost,
            )

            assert excinfo.code == HTTPStatus.BAD_REQUEST
            assert "no state" in excinfo.reason

    assert isinstance(httpd.error, RedirectionError)


@pytest.mark.timeout(2)
@pytest.mark.parametrize("scheme", ["http", "https"])
@pytest.mark.parametrize("missing_code", [{}, {"code": ""}])
@pytest.mark.asyncio
async def test_handle_authorization_code_no_code(
    event_loop, unused_tcp_port, scheme, missing_code, tls_context_localhost
):
    redirect_uri = f"{scheme}://127.0.0.1:{unused_tcp_port}"

    with redirection_server(redirect_uri) as httpd:
        event_loop.run_in_executor(None, httpd.handle_request)

        with pytest.raises(HTTPError) as excinfo:
            urlopen(
                f"{redirect_uri}"
                f"?{urlencode({**missing_code, 'state': httpd.state})}",
                timeout=5.0,
                context=tls_context_localhost,
            )

            assert excinfo.code == HTTPStatus.BAD_REQUEST
            assert "no code" in excinfo.reason

    assert isinstance(httpd.error, RedirectionError)


@pytest.mark.timeout(2)
@pytest.mark.asyncio
async def test_start_authorization_code_flow_ok(event_loop, tls_context_localhost):
    expected_code = random_string()

    def simulate_open_authorization_endpoint_ok(
        client_id, redirect_uri, state, *args, **kwargs
    ):
        event_loop.run_in_executor(
            None,
            partial(
                urlopen,
                f"{redirect_uri}?{urlencode({'code': expected_code, 'state': state})}",
                timeout=5.0,
                context=tls_context_localhost,
            ),
        )

    with (
        patch(
            "oidc_client.oauth.open_authorization_endpoint",
            side_effect=simulate_open_authorization_endpoint_ok,
        ),
    ):
        assert expected_code == start_authorization_code_flow(
            endpoint="https://example.com/oauth2/authorize",
            client_id="my-app",
            redirect_uri="https://localhost:39303",
            scope="openid profile email",
            pkce_secret=PKCESecret(),
        )


@pytest.mark.timeout(2)
@pytest.mark.asyncio
async def test_start_authorization_code_flow_error(event_loop, tls_context_localhost):
    def simulate_open_authorization_endpoint_error(
        client_id, redirect_uri, state, *args, **kwargs
    ):
        bad_state = random_string()
        assert state != bad_state
        event_loop.run_in_executor(
            None,
            partial(
                urlopen,
                f"{redirect_uri}?"
                f"{urlencode({'code': random_string(), 'state': bad_state})}",
                timeout=5.0,
                context=tls_context_localhost,
            ),
        )

    with patch(
        "oidc_client.oauth.open_authorization_endpoint",
        side_effect=simulate_open_authorization_endpoint_error,
    ), pytest.raises(RedirectionError):
        start_authorization_code_flow(
            endpoint="https://example.com/oauth2/authorize",
            client_id="my-app",
            redirect_uri="https://localhost:39303",
            scope="openid profile email",
            pkce_secret=PKCESecret(),
        )


@pytest.mark.parametrize(
    "redirect_uri",
    [
        "http://127.0.0.1",
        "https://localhost",
        "https://localhost/oauth2/callback",
        "http://127.0.0.1/oauth2/callback",
        "http://:39303",
        "https://:39303",
        "http://:39303/oauth2/callback",
        "https://:39303/oauth2/callback",
    ],
)
def test_validate_redirect_uri_no_hostname_port(redirect_uri):
    with pytest.raises(ValueError) as excinfo:
        redirection_server(redirect_uri)
    assert "cannot start redirection server" in str(excinfo.value)
